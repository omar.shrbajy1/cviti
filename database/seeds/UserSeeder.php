<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'first_name' => 'omar',
            'second_name' => 'Farouq',
            'last_name' => 'shrbaji',
            'email' => 'omar.shrbajy1@gmail.com',
            'password' => bcrypt('2131636+'),
            'username' => 'omarsh',
            'home_no' => '2113316',
            'about_me' => 'I am studying software engineering at Yarmouk private university with AGPA 3.15/4.00, and
I have over one and a half year of experience in analyzing, designing, programming and
testing software across variety of platforms. I am focusing on improving my techniques in
web development using various number of technologies including:
• Spring Framework (Java)
• Laravel Framework (PHP)
• Nodejs with Express
',

            'date_of_birth' => '1996-3-13',
            'gender' => 'male'
        ]);
    }
}
