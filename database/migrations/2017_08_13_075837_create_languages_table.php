<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('name');
            $table->string('native_name')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('language_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned() ;
            $table->integer('language_id')->unsigned() ;
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->primary(array('user_id', 'language_id'));
            $table->integer('rate');
            $table->timestamps();
            $table->softDeletes();

        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
        Schema::dropIfExists('language_user');
    }
}
