<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media_title');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('social_media_user', function (Blueprint $table) {
            $table->integer('social_media_id')->unsigned();
            $table->foreign('social_media_id')->references('id')->on('social_media')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->primary(array('social_media_id', 'user_id'));
            $table->string('media_url')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('project_social_media', function (Blueprint $table) {
            $table->integer('social_media_id')->unsigned();
            $table->foreign('social_media_id')->references('id')->on('social_media')->onDelete('cascade');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->primary(array('social_media_id', 'project_id'));
            $table->string('media_url');
            $table->timestamps();
            $table->softDeletes();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_media');
        Schema::dropIfExists('project_social_media');
        Schema::dropIfExists('social_media_user');
    }
}
