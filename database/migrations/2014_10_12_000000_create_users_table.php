<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('last_name');
            $table->string('username');
            $table->string('gender');
            $table->date('date_of_birth')->nullable();
            $table->string('email')->unique()->index();
            $table->text('about_me');
            $table->string('martial_status')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('home_no')->nullable();
            $table->string('office_no')->nullable();
            $table->string('address')->nullable();
            $table->text('cover_letter')->nullable();
            $table->string('img_url')->nullable()->default()->default('/src/images/default_profile_original_picture.png');
            $table->string("password");
            $table->string('facebook')->nullable();
            $table->string('github')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('youtube')->nullable();
            $table->string('twitter')->nullable();
            $table->string('stackoverflow')->nullable();
            $table->string('work_email')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('visitors');
        Schema::dropIfExists('education');
        Schema::dropIfExists('education_user');
        Schema::dropIfExists('interests');
        Schema::dropIfExists('interest_user');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('language_user');
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_user');
        Schema::dropIfExists('skills');
        Schema::dropIfExists('skill_user');
        Schema::dropIfExists('social_media');
        Schema::dropIfExists('project_social_media');
        Schema::dropIfExists('social_media_user');
    }
}
