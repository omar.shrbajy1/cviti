<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/x', function () {
    $ip = request()->ip();
    $data = (array)\Location::get($ip);

    $countryCode = $data['countryCode'];
    $countryName = $data['countryName'];
    $cityName = $data['cityName'];
    try {
        \App\Visitor::create([
            'country_code' => $countryCode,
            'country_name' => $countryName,
            'city_name' => $cityName,
            'ip' => $ip,
            'date' => date("Y-m-d")
        ]);
    } catch (\Exception $e) {

    }

//    return $data['countryCode'];
    dd($data);
});

Route::group(['middleware' => ['NewVisitor']], function () {
    Route::get('/', 'LandingController@index');

    Route::get('/cover-letter', 'LandingController@coverLetter');

    Route::get('/project', 'LandingController@project');

    Route::get('/contact', 'LandingController@contact');
    Route::post('/contact', 'LandingController@sendEmail');
});


Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {


    Route::Resource('/work-experience', 'WorkExperienceController');
    Route::post('/datatable/work-experience', 'WorkExperienceController@DataTable');


    Route::Resource('/project', 'ProjectController');
    Route::post('/datatable/project', 'ProjectController@DataTable');


    Route::Resource('/education', 'EducationController');
    Route::post('/datatable/education', 'EducationController@DataTable');


    Route::get('/user', 'UserController@index');
    Route::post('/user', 'UserController@update');


    Route::Resource('/', 'StatisticController');


    Route::get('/cover-letter', 'UserController@coverLetter');
    Route::post('/cover-letter', 'UserController@updateCoverLetter');


    Route::Resource('/skill', 'SkillController');
    Route::post('/datatable/skill', 'SkillController@DataTable');

});


Route::get('/home', 'HomeController@index')->name('home');
