@extends('admin.admin-master')


@section('title' , 'Personal Info')


@section('content')
    <div class="row">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Personal Info</h4>
            </div>
            <div class="panel-body">

                <form action="/admin/user" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" value="{{Auth::user()->email}}">
                    </div>
                    <div class="form-group">
                        <label for="pwd">First Name:</label>
                        <input type="text" name="first_name" class="form-control" value="{{Auth::user()->first_name}}">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Middle Name:</label>
                        <input type="text" name="second_name" class="form-control"
                               value="{{Auth::user()->second_name}}">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Last Name:</label>
                        <input type="text" name="last_name" class="form-control" value="{{Auth::user()->last_name}}">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Mobile No:</label>
                        <input type="text" name="mobile_no" class="form-control" value="{{Auth::user()->mobile_no}}">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Home NO:</label>
                        <input type="text" name="home_no" class="form-control" value="{{Auth::user()->home_no}}">
                    </div>

                    <div class="form-group">
                        <label for="pwd">About Me:</label>
                        <textarea name="about_me" class="form-control">  {{Auth::user()->about_me}} </textarea>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Address:</label>
                        <input name="address" class="form-control" value="{{Auth::user()->address}}">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Birth Date:</label>
                        <input type="date" name="date_of_birth" class="form-control"
                               value="{{Auth::user()->date_of_birth}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Martial Status:</label>
                        <input type="text" name="martial_status" class="form-control"
                               value="{{Auth::user()->martial_status}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Facebook</label>
                        <input type="text" name="facebook" class="form-control"
                               value="{{Auth::user()->facebook}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Github</label>
                        <input type="text" name="github" class="form-control"
                               value="{{Auth::user()->github}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">LinkedIn</label>
                        <input type="text" name="linkedin" class="form-control"
                               value="{{Auth::user()->linkedin}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Stack overflow</label>
                        <input type="text" name="stackoverflow" class="form-control"
                               value="{{Auth::user()->stackoverflow}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Twitter</label>
                        <input type="text" name="twitter" class="form-control"
                               value="{{Auth::user()->twitter}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Youtube</label>
                        <input type="text" name="youtube" class="form-control"
                               value="{{Auth::user()->youtube}}">
                    </div>
                    <div class=" form-group">
                        <label for="pwd">Work - Email</label>
                        <input type="text" name="work_email" class="form-control"
                               value="{{Auth::user()->work_email}}">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>


            </div>
        </div>
    </div>


@endsection


@section('scripts')


@endsection