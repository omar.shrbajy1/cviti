<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
        <div data-scrollbar="true" data-height="100%" data-init="true"
             style="overflow: hidden; width: auto; height: 100%;">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <div class="image">
                    </div>
                    <div class="info">
                        {{Auth::user()->first_name . ' ' . Auth::user()->last_name}}

                    </div>
                </li>
            </ul>
            <!-- end sidebar user -->
            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">Navigation</li>
                <li id="dashboard"><a href="{{url('/admin')}}"> <span>Statistics</span></a>

                <li id="subscriber"><a href="{{url('/admin/work-experience')}}"> <span>Work Experience</span></a>
                </li>
                <li id="subscriber-type"><a href="{{url('/admin/project')}}"> <span>Projects</span></a>

                <li id="companies"><a href="{{url('/admin/education')}}"> <span>Education</span></a>
                </li>

                <li id="accessory"><a href="{{url('/admin/user')}}">Personal Info</a></li>

                <li id="accessory_orders"><a href="{{url('/admin/cover-letter')}}">Cover Letter</a></li>

                <li id="material"><a href="/admin/skill">Skill</a></li>

                <!-- begin sidebar minify button -->

                <!-- end sidebar minify button -->
            </ul>
            <!-- end sidebar nav -->
        </div>
        <div class="slimScrollBar"
             style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 294.758px;"></div>
        <div class="slimScrollRail"
             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
    </div>
    <!-- end sidebar scrollbar -->
</div>