@extends('admin.admin-master')


@section('title' , 'Skill')


@section('content')
    <div class="row">
        <div class="modal fade" id="add-skill-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Skill</h4>
                    </div>
                    <form id="add_skill_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Title</label>
                                <input type="text" class="form-control" name="skill_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Rate</label>
                                <input type="text" class="form-control" name="rate">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>


        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Skill</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" id="skill_table" class="table table-bordered table-stripped ">
                        <thead>
                        <tr>
                            <th>Skill Name</th>
                            <th>Percentage</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>

                            <th><i data-toggle="modal" data-target="#add-skill-modal" style="cursor:pointer"
                                   class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')

    <script>
        (function () {
            var csrf = $('meta[name=csrf-token]').attr('content');
            var addSkill = $('#add_skill_form');


            var skillRowId = 0;


            function confirm(msg, content, cb) {
                $.confirm({
                    title: msg,
                    content: content,
                    buttons: {
                        'delete': {
                            action: function () {
                                cb();
                            },
                            btnClass: 'btn-red'
                        },
                        'cancel': function () {

                        }

                    }
                });

            }

            $('#skill_table tbody').on('click', '.fa-trash-o', function () {
                var data = (skillDataTable.row($(this).parents('tr')).data());

                confirm('Delete Skill', 'Are you sure you want to delete Skill', function () {

                    $.ajax({
                        url: '/admin/skill/' + data.id,
                        type: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': csrf
                        },
                        success: function () {
                            skillDataTable.draw();


                        }
                    })
                })


            });


            addSkill.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    url: '/admin/skill',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    data: {
                        skill_title: addSkill.find('input[name=skill_title]').val(),
                        rate: addSkill.find('input[name=rate]').val()
                    },

                    success: function (data) {
                        skillDataTable.draw();
                        $('#add-skill-modal').modal('hide')
                    }

                })


            });


            var skillDataTable = $('#skill_table').DataTable({
                responsive: !0,
                processing: true,
                serverSide: true,
                ajax: {
                    type: 'post',
                    url: '/admin/datatable/skill',
                    'headers': {
                        'X-CSRF-TOKEN': csrf
                    }
                },
                columns: [
                    {
                        data: function (data) {
                            return data.skill_title
                        },
                        name: 'skill_title'

                    },
                    {
                        data: function (data) {
                            return data.rate
                        },
                        name: 'rate'

                    },
                    {
                        data: function (data) {
                            return data.action
                        },
                        name: 'action'

                    }
                ]

            })
        }())


    </script>
@endsection