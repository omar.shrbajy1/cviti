@extends('admin.admin-master')

@section('styles')


@endsection

@section('title' , 'Cover Letter')

@section('content')

    <div class="row">
        <form action="/admin/cover-letter" method="post">
            <textarea name="cover_letter"> {{Auth::user()->cover_letter}} </textarea>
            {{csrf_field()}}
            <br>
            <button class="btn btn-block btn-primary" type="submit">Update</button>
        </form>
    </div>


@endsection




@section('scripts')

    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('cover_letter');
    </script>
@endsection