<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta content="{{csrf_token()}}" name="csrf-token"/>
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="{{asset('/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-responsive.min.css')}}">
    <link href="{{asset('css/default.css')}}" rel="stylesheet" id="theme">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <meta content="{{csrf_token()}}" name="csrf-token"/>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    @yield('styles')
    <title>@yield('title')</title>
</head>
<body>
<div id="page-container"
     class="   page-sidebar-fixed page-header-fixed   in ">
    @include('admin.includes.header')
    @include('admin.includes.sidebar')
    <div id="content" class="content">

        @yield('content')
    </div>

</div>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/js/apps.min.js"></script>
<script src="/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
{{--<script src="/js-template/table-manage-buttons.demo.min.js"></script>--}}
<script src="{{asset('js/events.js')}}"></script>
<script src="{{asset('js/pace.min.js')}}"></script>
@yield('scripts')
</body>
</html>