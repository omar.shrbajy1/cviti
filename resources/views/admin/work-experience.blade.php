@extends('admin.admin-master')


@section('title', 'Work Experience')


@section('content')

    <div class="row">
        <div class="modal fade" id="add-work-experience-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Work Experience</h4>
                    </div>
                    <form id="add_work_experience_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Job Title</label>
                                <input type="text" class="form-control" name="job_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Company Name</label>
                                <input type="text" class="form-control" name="company_name" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Started At</label>
                                <input type="date" class="form-control" name="started_at" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Ended At</label>
                                <input type="date" class="form-control" name="ended_at">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>


        <div class="modal fade" id="edit-work-experience-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Work Experience</h4>
                    </div>
                    <form id="edit-work-experience-form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Job Title</label>
                                <input type="text" class="form-control" name="job_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Company Name</label>
                                <input type="text" class="form-control" name="company_name" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Started At</label>
                                <input type="date" class="form-control" name="started_at" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Ended At</label>
                                <input type="date" class="form-control" name="ended_at">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Edit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>


        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Work Experience</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" id="work_experience_table" class="table table-bordered table-stripped ">
                        <thead>
                        <tr>
                            <th>Job Title</th>
                            <th>Company Name</th>
                            <th>Description</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><i data-toggle="modal" data-target="#add-work-experience-modal" style="cursor:pointer"
                                   class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>

        (function () {
            var csrf = $('meta[name=csrf-token]').attr('content');
            var addWorkExperience = $('#add_work_experience_form');
            var editWorkExperience = $('#edit-work-experience-form')
            var editWorkExperienceModal = $('#edit-work-experience-modal');
            var workExperienceRowId = 0;

            $('#work_experience_table tbody').on('click', '.fa-pencil', function () {
                var data = (workExperienceDataTable.row($(this).parents('tr')).data());
                editWorkExperienceModal.find('input[name=job_title]').val(data.job_title);
                editWorkExperienceModal.find('input[name=company_name]').val(data.company_name);
                editWorkExperienceModal.find('input[name=started_at]').val(data.started_at);
                editWorkExperienceModal.find('input[name=ended_at]').val(data.ended_at);
                editWorkExperienceModal.find('textarea').val(data.description)
                workExperienceRowId = data.id;
                editWorkExperienceModal.modal('show')
            });


            editWorkExperience.on('submit', function (e) {

                e.preventDefault();

                $.ajax({

                    url: '/admin/work-experience/' + workExperienceRowId,
                    type: 'put',
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    data: {
                        job_title: editWorkExperienceModal.find('input[name=job_title]').val(),
                        'company_name': editWorkExperienceModal.find('input[name=company_name]').val(),
                        started_at: editWorkExperienceModal.find('input[name=started_at]').val(),
                        ended_at: editWorkExperienceModal.find('input[name=ended_at]').val(),
                        description: editWorkExperienceModal.find('textarea').val()
                    },
                    success: function () {
                        editWorkExperienceModal.modal('hide');
                        workExperienceDataTable.draw();

                    }

                })


            })


            function confirm(msg, content, cb) {
                $.confirm({
                    title: msg,
                    content: content,
                    buttons: {
                        'delete': {
                            action: function () {
                                cb();
                            },
                            btnClass: 'btn-red'
                        },
                        'cancel': function () {

                        }

                    }
                });

            }

            $('#work_experience_table tbody').on('click', '.fa-trash-o', function () {
                var data = (workExperienceDataTable.row($(this).parents('tr')).data());

                confirm('Delete Work Experience', 'Are you sure you want to delete Work experience', function () {

                    $.ajax({
                        url: '/admin/work-experience/' + data.id,
                        type: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': csrf
                        },
                        success: function () {
                            workExperienceDataTable.draw();


                        }
                    })
                })


            });


            addWorkExperience.on('submit', function (e) {
                e.preventDefault();

                $.ajax({

                    url: '/admin/work-experience',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    data: {
                        job_title: addWorkExperience.find('input[name=job_title]').val(),
                        company_name: addWorkExperience.find('input[name=company_name]').val(),
                        started_at: addWorkExperience.find('input[name=started_at]').val(),
                        ended_at: addWorkExperience.find('input[name=ended_at]').val(),
                        description: addWorkExperience.find('textarea').val()
                    },

                    success: function (data) {
                        workExperienceDataTable.draw();
                        $('#add-work-experience-modal').modal('hide')
                    }

                })


            });


            var workExperienceDataTable = $('#work_experience_table').DataTable({
                responsive: !0,
                processing: true,
                serverSide: true,
                ajax: {
                    type: 'post',
                    url: '/admin/datatable/work-experience',
                    'headers': {
                        'X-CSRF-TOKEN': csrf
                    }
                },
                columns: [
                    {
                        data: function (data) {
                            return data.job_title
                        },
                        name: 'job_title'

                    },
                    {
                        data: function (data) {
                            return data.company_name
                        },
                        name: 'company'

                    },
                    {
                        data: function (data) {
                            return data.description || '-'
                        },
                        name: 'description'

                    },
                    {
                        data: function (data) {
                            return data.started_at;
                        },
                        name: 'started_at'
                    },
                    {
                        data: function (data) {

                            return data.ended_at || '-';
                        },
                        name: 'ended_at'
                    },
                    {
                        data: function (data) {
                            return data.action;
                        },
                        name: 'action'
                    }
                ]

            })
        }())


    </script>
@endsection