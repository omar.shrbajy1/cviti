@extends('admin.admin-master')


@section('title' , 'Statistics')


@section('content')
    <div class="row">
        <div class="modal fade" id="add-education-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Education</h4>
                    </div>
                    <form id="add_education_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Education Title</label>
                                <input type="text" class="form-control" name="education_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Scientific Institution</label>
                                <input type="text" class="form-control" name="scientific_institution">
                            </div>
                            <div class="form-group">
                                <label for="room">Degree</label>
                                <input class="form-control" name="degree">
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Started At</label>
                                <input type="date" class="form-control" name="started_at" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Ended At</label>
                                <input type="date" class="form-control" name="ended_at">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
        <div class="modal fade" id="edit-education-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Education</h4>
                    </div>
                    <form id="edit_education_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Education Title</label>
                                <input type="text" class="form-control" name="education_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Scientific Institution</label>
                                <input type="text" class="form-control" name="scientific_institution">
                            </div>
                            <div class="form-group">
                                <label for="room">Degree</label>
                                <input class="form-control" name="degree">
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Started At</label>
                                <input type="date" class="form-control" name="started_at" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Ended At</label>
                                <input type="date" class="form-control" name="ended_at">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Edit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>


        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Visitor</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" id="education_table" class="table table-bordered table-stripped ">
                        <thead>
                        <tr>
                            <th>IP</th>
                            <th>Country Code</th>
                            <th>Country Name</th>
                            <th>City Name</th>
                            <th>Date</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Visitor::all() as $item)
                            <tr>

                                <th>{{$item->ip}}</th>
                                <th>{{$item->country_code}}</th>
                                <th>{{$item->country_name}}</th>
                                <th>{{$item->city_name}}</th>
                                <th>{{$item->date}}</th>

                            </tr>

                        @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')

    <script>

@endsection