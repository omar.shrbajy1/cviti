@extends('admin.admin-master')


@section('title', 'Projects')


@section('content')

    <div class="row">
        <div class="modal fade" id="add-project-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Work Experience</h4>
                    </div>
                    <form id="add_project_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Project Name</label>
                                <input type="text" class="form-control" name="project_name" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Project URL</label>
                                <input type="url" class="form-control" name="project_url">
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Image</label>
                                <input type="file" class="form-control" name="photo">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>


        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Project</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" id="project_table" class="table table-bordered table-stripped ">
                        <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Project URL</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><i data-toggle="modal" data-target="#add-project-modal" style="cursor:pointer"
                                   class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>

        (function () {
            var csrf = $('meta[name=csrf-token]').attr('content');
            var addProject = $('#add_project_form');
            var editWorkExperience = $('#edit-work-experience-form')
            var editWorkExperienceModal = $('#edit-work-experience-modal');
            var workExperienceRowId = 0;

//            $('#work_experience_table tbody').on('click', '.fa-pencil', function () {
//                var data = (workExperienceDataTable.row($(this).parents('tr')).data());
//                editWorkExperienceModal.find('input[name=job_title]').val(data.job_title);
//                editWorkExperienceModal.find('input[name=company_name]').val(data.company_name);
//                editWorkExperienceModal.find('input[name=started_at]').val(data.started_at);
//                editWorkExperienceModal.find('input[name=ended_at]').val(data.ended_at);
//                editWorkExperienceModal.find('textarea').val(data.description)
//                workExperienceRowId = data.id;
//                editWorkExperienceModal.modal('show')
//            });


//            editWorkExperience.on('submit', function (e) {
//
//                e.preventDefault();
//
//                $.ajax({
//
//                    url: '/admin/work-experience/' + workExperienceRowId,
//                    type: 'put',
//                    headers: {
//                        'X-CSRF-TOKEN': csrf
//                    },
//                    data: {
//                        job_title: editWorkExperienceModal.find('input[name=job_title]').val(),
//                        'company_name': editWorkExperienceModal.find('input[name=company_name]').val(),
//                        started_at: editWorkExperienceModal.find('input[name=started_at]').val(),
//                        ended_at: editWorkExperienceModal.find('input[name=ended_at]').val(),
//                        description: editWorkExperienceModal.find('textarea').val()
//                    },
//                    success: function () {
//                        editWorkExperienceModal.modal('hide');
//                        workExperienceDataTable.draw();
//
//                    }
//
//                })
//
//
//            })


            function confirm(msg, content, cb) {
                $.confirm({
                    title: msg,
                    content: content,
                    buttons: {
                        'delete': {
                            action: function () {
                                cb();
                            },
                            btnClass: 'btn-red'
                        },
                        'cancel': function () {

                        }

                    }
                });

            }

            $('#project_table tbody').on('click', '.fa-trash-o', function () {
                var data = (projectDataTable.row($(this).parents('tr')).data());

                confirm('Delete Project', 'Are you sure you want to delete project', function () {

                    $.ajax({
                        url: '/admin/project/' + data.id,
                        type: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': csrf
                        },
                        success: function () {
                            projectDataTable.draw();


                        }
                    })
                })


            });


            addProject.on('submit', function (e) {
                e.preventDefault();
                var self = this;
                var data = new FormData(self);
                $.ajax({
                    url: "/admin/project",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    'headers': {
                        'X-CSRF-TOKEN': csrf
                    },
                    processData: false,
                    success: function (data) {
                        projectDataTable.draw();
                        $('#add-project-modal').modal('hide');
                        console.log(data);
                    }

                })


            });


            var projectDataTable = $('#project_table').DataTable({
                responsive: !0,
                processing: true,
                serverSide: true,
                ajax: {
                    type: 'post',
                    url: '/admin/datatable/project',
                    'headers': {
                        'X-CSRF-TOKEN': csrf
                    }
                },
                columns: [
                    {
                        data: function (data) {
                            return data.project_name
                        },
                        name: 'project_name'

                    },
                    {
                        data: function (data) {
                            if (data.project_url)
                                return '<a target="_blank" href="' + data.project_url + '">' + data.project_url + '</a>';
                            return '-';
                        },
                        name: 'project_url'

                    },
                    {
                        data: function (data) {
                            return data.description || '-'
                        },
                        name: 'description'

                    },
                    {
                        data: function (data) {
                            if (data.image)
                                return '<img src="' + data.image + '">';

                            return '-'
                        },
                        name: 'image'
                    },
                    {
                        data: function (data) {
                            return data.action;
                        },
                        name: 'action'
                    }
                ]
            })
        }())


    </script>
@endsection