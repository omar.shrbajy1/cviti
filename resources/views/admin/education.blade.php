@extends('admin.admin-master')


@section('title' , 'Education')


@section('content')
    <div class="row">
        <div class="modal fade" id="add-education-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Education</h4>
                    </div>
                    <form id="add_education_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Education Title</label>
                                <input type="text" class="form-control" name="education_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Scientific Institution</label>
                                <input type="text" class="form-control" name="scientific_institution">
                            </div>
                            <div class="form-group">
                                <label for="room">Degree</label>
                                <input class="form-control" name="degree">
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Started At</label>
                                <input type="date" class="form-control" name="started_at" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Ended At</label>
                                <input type="date" class="form-control" name="ended_at">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
        <div class="modal fade" id="edit-education-modal" role="dialog">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Education</h4>
                    </div>
                    <form id="edit_education_form">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="room"> Education Title</label>
                                <input type="text" class="form-control" name="education_title" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Scientific Institution</label>
                                <input type="text" class="form-control" name="scientific_institution">
                            </div>
                            <div class="form-group">
                                <label for="room">Degree</label>
                                <input class="form-control" name="degree">
                            </div>
                            <div class="form-group">
                                <label for="room">Description</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="room">Started At</label>
                                <input type="date" class="form-control" name="started_at" required>
                            </div>
                            <div class="form-group">
                                <label for="room">Ended At</label>
                                <input type="date" class="form-control" name="ended_at">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Edit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>


        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Education</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" id="education_table" class="table table-bordered table-stripped ">
                        <thead>
                        <tr>
                            <th>Education Title</th>
                            <th>Scientific Institution</th>
                            <th>Degree</th>
                            <th>Description</th>
                            <th>Started At</th>
                            <th>Ended At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><i data-toggle="modal" data-target="#add-education-modal" style="cursor:pointer"
                                   class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')

    <script>
        (function () {
            var csrf = $('meta[name=csrf-token]').attr('content');
            var addEducation = $('#add_education_form');

            var editEducation = $('#edit_education_form')
            var editEducationModal = $('#edit-education-modal');
            var educationRowId = 0;

            $('#education_table tbody').on('click', '.fa-pencil', function () {
                var data = (educationDataTable.row($(this).parents('tr')).data());
                editEducationModal.find('input[name=education_title]').val(data.education_title);
                editEducationModal.find('input[name=scientific_institution]').val(data.scientific_institution);
                editEducationModal.find('input[name=degree]').val(data.degree);
                editEducationModal.find('textarea[name=description]').val(data.description);
                editEducationModal.find('input[name=started_at]').val(data.started_at)
                editEducationModal.find('input[name=ended_at]').val(data.ended_at)
                educationRowId = data.id;
                editEducationModal.modal('show')
            });


            editEducation.on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '/admin/education/' + educationRowId,
                    type: 'put',
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    data: {
                        education_title: editEducation.find('input[name=education_title]').val(),
                        scientific_institution: editEducation.find('input[name=scientific_institution]').val(),
                        degree: editEducation.find('input[name=degree]').val(),
                        description: editEducation.find('textarea').val(),
                        started_at: editEducation.find('input[name=started_at]').val(),
                        ended_at: editEducation.find('input[name=ended_at]').val()
                    },
                    success: function () {
                        editEducationModal.modal('hide');
                        educationDataTable.draw();

                    }

                })


            })


            function confirm(msg, content, cb) {
                $.confirm({
                    title: msg,
                    content: content,
                    buttons: {
                        'delete': {
                            action: function () {
                                cb();
                            },
                            btnClass: 'btn-red'
                        },
                        'cancel': function () {

                        }

                    }
                });

            }

            $('#education_table tbody').on('click', '.fa-trash-o', function () {
                var data = (educationDataTable.row($(this).parents('tr')).data());

                confirm('Delete Education', 'Are you sure you want to delete Education', function () {

                    $.ajax({
                        url: '/admin/education/' + data.id,
                        type: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': csrf
                        },
                        success: function () {
                            educationDataTable.draw();


                        }
                    })
                })


            });


            addEducation.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    url: '/admin/education',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    data: {
                        education_title: addEducation.find('input[name=education_title]').val(),
                        scientific_institution: addEducation.find('input[name=scientific_institution]').val(),
                        degree: addEducation.find('input[name=degree]').val(),
                        description: addEducation.find('textarea').val(),
                        started_at: addEducation.find('input[name=started_at]').val(),
                        ended_at: addEducation.find('input[name=ended_at]').val()
                    },

                    success: function (data) {
                        educationDataTable.draw();
                        $('#add-education-modal').modal('hide')
                    }

                })


            });


            var educationDataTable = $('#education_table').DataTable({
                responsive: !0,
                processing: true,
                serverSide: true,
                ajax: {
                    type: 'post',
                    url: '/admin/datatable/education',
                    'headers': {
                        'X-CSRF-TOKEN': csrf
                    }
                },
                columns: [
                    {
                        data: function (data) {
                            return data.education_title
                        },
                        name: 'education_title'

                    },
                    {
                        data: function (data) {
                            return data.scientific_institution
                        },
                        name: 'scientific_institution'

                    },
                    {
                        data: function (data) {
                            return data.degree
                        },
                        name: 'degree'

                    },
                    {
                        data: function (data) {
                            return data.description;
                        },
                        name: 'description'
                    },
                    {
                        data: function (data) {

                            return data.started_at;
                        },
                        name: 'started_at'
                    },
                    {
                        data: function (data) {

                            return data.ended_at || '-';
                        },
                        name: 'ended_at'
                    },
                    {
                        data: function (data) {
                            return data.action;
                        },
                        name: 'action'
                    }
                ]

            })
        }())


    </script>
@endsection