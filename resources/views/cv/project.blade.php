<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">

<!-- Mirrored from demo.deviserweb.com/cv/project.blade.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 20:46:14 GMT -->
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE-->
    <title> Projects </title>

    <!-- META TAG -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="CV, Portfolio, Resume">
    <meta name="author" content="Md. Siful Islam, Desiver Web">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.html">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">

    <!-- ========================================
            Stylesheets
    ==========================================-->

    <!-- MATERIALIZE CORE CSS -->
    <link href="assets/css/materialize.min.css" rel="stylesheet">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/animate.css">


    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet'
          type='text/css'>


    <!--FONTAWESOME CSS-->
    <link href="assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- CUSTOM STYLE -->
    <link href="assets/css/nav.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- RESPONSIVE CSS-->
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- COLORS -->
    <link rel="alternate stylesheet" href="assets/css/colors/red.css" title="red">
    <link rel="alternate stylesheet" href="assets/css/colors/purple.css" title="purple">
    <link rel="alternate stylesheet" href="assets/css/colors/orange.css" title="orange">
    <link rel="alternate stylesheet" href="assets/css/colors/green.css" title="green">
    <link rel="stylesheet" href="assets/css/colors/lime.css" title="lime">


    <!-- STYLE SWITCH STYLESHEET ONLY FOR DEMO -->
    <link rel="stylesheet" href="assets/css/demo.css">

    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif] -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Start Container-->
<div class="container">
    <!-- row -->
    <div class="row">
        <!-- =========================================
                       SIDEBAR
        ==========================================-->
        <!-- Start Sidebar -->
    @include('cv.includes.sidebar')


    <!-- =========================================
           Work experiences
        ==========================================-->

        <section class="col s12 m12 l8 section">
            <div class="row">
                <div class="full-portfolio">
                    <div class="wow fadeIn a1" data-wow-delay="0.1s">
                        <div class="portfolio-nav section-wrapper z-depth-1">

                        </div>
                        <div id="loader">
                            <div class="loader-icon"></div>
                        </div>

                        <div class="screenshots" id="portfolio-item">
                            <div class="row">
                                <ul class="grid">
                                    <!-- Portfolio one-->
                                    @foreach(\App\Project::all() as $item)
                                        <li class="col m6 s1 2 mix category-1">
                                            <a target="_blank" href="{{$item->project_url}}"
                                               class="sa-view-project-detail">
                                                <figure class="more">
                                                    <img src="{{$item->image}}" alt="Screenshot 01" class="">
                                                    <figcaption>
                                                        <div class="caption-content">
                                                            <div class="single_image">
                                                                <h2>{{$item->project_title}}</h2>
                                                                <p>{{$item->description}}</p>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <!-- PROJECT DETAILS WILL BE LOADED HERE -->
                        <div class="sa-project-gallery-view" id="project-gallery-view"></div>
                        <div class="back-btn col s12">
                            <a id="back-button" class="btn btn-info waves-effect" href="#"><i
                                        class="fa fa-long-arrow-left"></i> Go Back </a>
                        </div>
                    </div>
                </div>


                <!-- =======================================
                  portfolio Website
                ==========================================-->
                <div class="clear"></div>
                <div class="section-wrapper z-depth-1 wow fadeIn" data-wow-delay="0.1s">
                    <div class="col s12 m12 l10 website right">
                        <div class="row">
                            <div class="col s12 m12 l6">
                                <span><a href="#">www.alrayhan.com</a></span>
                            </div>
                            <div class="col col s12 m12 l6">
                                <span><a href="#">www.dribble.com/rtralrayhan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
        </section><!-- end section -->
    </div> <!-- end row -->
</div>  <!-- end container -->

<!--=====================
               JavaScript
       ===================== -->
<!-- Jquery core js-->
<script src="assets/js/jquery.min.js"></script>

<!-- materialize js-->
<script src="assets/js/materialize.min.js"></script>

<!-- wow js-->
<script src="assets/js/wow.min.js"></script>

<!-- Map api -->
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<!-- Masonry js-->

<script src="assets/js/validator.min.js"></script>

<script src="assets/js/jquery.mixitup.min.js"></script>

<!-- Customized js -->
<script src="assets/js/init.js"></script>

<!-- =========================================================
    STYLE SWITCHER | ONLY FOR DEMO NOT INCLUDED IN MAIN FILES
===========================================================-->

<!-- Style switter js -->

<div class="cv-style-switch" id="switch-style">
    <a id="toggle-switcher" class="switch-button icon_tools"> <i class="fa fa-cogs"></i></a>
    <div class="switched-options">
        <div class="config-title">
            Colors :
        </div>
        <ul class="styles">
            <li><a href="index.blade.php#" onclick="setActiveStyleSheet('red'); return false;" title="Red">
                    <div class="red"></div>
                </a></li>

            <li><a href="index.blade.php#" onclick="setActiveStyleSheet('purple'); return false;" title="purple">
                    <div class="purple"></div>
                </a></li>

            <li><a href="index.blade.php#" onclick="setActiveStyleSheet('orange'); return false;" title="orange">
                    <div class="orange"></div>
                </a></li>

            <li><a href="index.blade.php#" onclick="setActiveStyleSheet('green'); return false;" title="green">
                    <div class="green"></div>
                </a></li>

            <li><a href="index.blade.php#" onclick="setActiveStyleSheet('lime'); return false;" title="lime">
                    <div class="lime"></div>
                </a></li>

            <li class="p">
                ( NOTE: Pre Defined Colors. You can change colors very easily )
            </li>
        </ul>
    </div>
</div>

</body>

<!-- Mirrored from demo.deviserweb.com/cv/project.blade.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 20:46:25 GMT -->
</html>
