<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">

<!-- Mirrored from demo.deviserweb.com/cv/index.blade.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 20:44:44 GMT -->
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE-->
    <title>     {{\App\User::first()->first_name . ' ' . \App\User::first()->second_name . ' ' . \App\User::first()->last_name}}
    </title>

    <!-- META TAG -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="CV, Portfolio, Resume">
    <meta name="author" content="Md. Siful Islam, Desiver Web">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.html">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">

    <!-- ========================================
            Stylesheets
    ==========================================-->

    <!-- MATERIALIZE CORE CSS -->
    <link href="assets/css/materialize.min.css" rel="stylesheet">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/animate.css">


    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet'
          type='text/css'>


    <!--FONTAWESOME CSS-->
    <link href="assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- CUSTOM STYLE -->
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- RESPONSIVE CSS-->
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- COLORS -->
    <link rel="alternate stylesheet" href="assets/css/colors/red.css" title="red">
    <link rel="alternate stylesheet" href="assets/css/colors/purple.css" title="purple">
    <link rel="alternate stylesheet" href="assets/css/colors/orange.css" title="orange">
    <link rel="alternate stylesheet" href="assets/css/colors/green.css" title="green">
    <link rel="stylesheet" href="assets/css/colors/lime.css" title="lime">


    <!-- STYLE SWITCH STYLESHEET ONLY FOR DEMO -->
    <link rel="stylesheet" href="assets/css/demo.css">

    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif] -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Start Container-->
<div class="container">
    <!-- row -->
    <div class="row">
        <!-- =========================================
                       SIDEBAR
        ==========================================-->
        <!-- Start Sidebar -->
    @include('cv.includes.sidebar')

    <!-- =========================================
           Work experiences
        ==========================================-->

        <section class="col s12 m12 l8 section">
            <div class="row">
                <div class="section-wrapper z-depth-1">
                    <div class="section-icon col s12 m12 l2">
                        <i class="fa fa-suitcase"></i>
                    </div>
                    <div class="custom-content col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">
                        <h2>Work Experience</h2>
                        @foreach(\App\WorkExperience::all() as $item)
                            <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                <h3>{{$item->job_title}} <span>@ {{ $item->company_name }}</span></h3>
                                <span>{{$item->started_at }} - {{$item->ended_at ? : 'Present'}} </span>
                                <p>{{$item->description }}</p>
                            </div>
                        @endforeach
                    </div>
                </div>

                <!-- ========================================
                 Education
                ==========================================-->

                <div class="section-wrapper z-depth-1">
                    <div class="section-icon col s12 m12 l2">
                        <i class="fa fa-graduation-cap"></i>
                    </div>
                    <div class="custom-content col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">
                        <h2>Education </h2>
                        @foreach(\App\Education::all() as $item)
                            <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                <h3>{{$item->education_title}} <span>@ {{$item->scientific_institution}}</span></h3>
                                <span>{{$item->started_at}} - {{$item->ended_at ? : 'Present'}} </span>
                                <p>{{$item->description}} </p>
                            </div>
                        @endforeach
                    </div>
                </div>

                <!-- ========================================
                      Intertests
                ==========================================-->

                <div class="section-wrapper z-depth-1">
                    <div class="section-icon col s12 m12 l2">
                        <i class="fa fa-plane"></i>
                    </div>
                    <div class="interests col s12 m12 l10 wow fadeIn" data-wow-delay="0.1s">
                        <h2>Social Media </h2>
                        <ul> <!-- interetsr icon start -->
                            <li><a target="_blank" href="{{\App\User::first()->facebook}}"><i
                                            class="fa  fa-facebook-square tooltipped" data-position="top"
                                            data-delay="50"
                                            data-tooltip="Facebook"></i></a></li>
                            <li><a target="_blank" href="{{\App\User::first()->github}}"><i
                                            class="fa fa-github-square tooltipped" data-position="top"
                                            data-delay="50"
                                            data-tooltip="Github"></i></a></li>
                            <li><a target="_blank" href="{{\App\User::first()->youtube}}"><i class="fa fa-youtube tooltipped"
                                                                                       data-position="top"
                                                                                       data-delay="50"
                                                                                       data-tooltip="Youtube"></i></a>
                            </li>
                            <li><a target="_blank" href="{{\App\User::first()->linkedin}}"><i
                                            class="fa fa-linkedin tooltipped" data-position="top" data-delay="50"
                                            data-tooltip="LinkedIn"></i></a></li>
                            <li><a target="_blank" href="{{\App\User::first()->twitter}}"><i
                                            class="fa fa-twitter-square tooltipped" data-position="top"
                                            data-delay="50"
                                            data-tooltip="Twitter"></i></a></li>
                        </ul> <!-- interetsr icon end -->
                    </div>
                </div>
                <!-- =======================================
                  portfolio Website
                ==========================================-->

                <div class="section-wrapper z-depth-1 wow fadeIn" data-wow-delay="0.1s">
                    <div class="col s12 m12 l10 website right">
                        <div class="row">
                            <div class="col s12 m12 l6">
                                <span><a href="#">www.alrayhan.com</a></span>
                            </div>
                            <div class="col col s12 m12 l6">
                                <span><a href="#">www.dribble.com/rtralrayhan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
        </section><!-- end section -->
    </div> <!-- end row -->
</div>  <!-- end container -->

<!--=====================
               JavaScript
       ===================== -->
<!-- Jquery core js-->
<script src="assets/js/jquery.min.js"></script>

<!-- materialize js-->
<script src="assets/js/materialize.min.js"></script>

<!-- wow js-->
<script src="assets/js/wow.min.js"></script>

<!-- Map api -->
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<!-- Masonry js-->

<script src="assets/js/validator.min.js"></script>

<!-- Customized js -->
<script src="assets/js/init.js"></script>

<!-- =========================================================
    STYLE SWITCHER | ONLY FOR DEMO NOT INCLUDED IN MAIN FILES
===========================================================-->

<!-- Style switter js -->


</body>

<!-- Mirrored from demo.deviserweb.com/cv/index.blade.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 20:44:57 GMT -->
</html>
