<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">

<!-- Mirrored from demo.deviserweb.com/cv/contact.blade.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 20:46:33 GMT -->
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE-->
    <title> Al Rayhan </title>

    <!-- META TAG -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="CV, Portfolio, Resume">
    <meta name="author" content="Desiver Web, Md. Siful Islam">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.html">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">

    <!-- ========================================
            Stylesheets
    ==========================================-->

    <!-- MATERIALIZE CORE CSS -->
    <link href="assets/css/materialize.min.css" rel="stylesheet">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/animate.css">


    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet'
          type='text/css'>


    <!--FONTAWESOME CSS-->
    <link href="assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- CUSTOM STYLE -->
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- RESPONSIVE CSS-->
    <link href="assets/css/responsive.css" rel="stylesheet">


    <!-- COLORS -->
    <link rel="alternate stylesheet" href="assets/css/colors/red.css" title="red">
    <link rel="alternate stylesheet" href="assets/css/colors/purple.css" title="purple">
    <link rel="alternate stylesheet" href="assets/css/colors/orange.css" title="orange">
    <link rel="alternate stylesheet" href="assets/css/colors/green.css" title="green">
    <link rel="stylesheet" href="assets/css/colors/lime.css" title="lime">


    <!-- STYLE SWITCH STYLESHEET ONLY FOR DEMO -->
    <link rel="stylesheet" href="assets/css/demo.css">

    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif] -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Start Container-->
<div class="container">
    <!-- row -->
    <div class="row">
        <!-- =========================================
                       SIDEBAR
        ==========================================-->
        <!-- Start Sidebar -->
    @include('cv.includes.sidebar')
    <!-- =========================================
                 Google Map
        ===========================================-->

        <section class="col s12 m12 l8 section">
            <div class="row">
                <!-- Start map -->
                <div class="section-wrapper g-map z-depth-1">
                    <div id="map"></div>
                </div>

                <!--=======================================
                 Contact
                ==========================================-->

                <div class="section-wrapper z-depth-1">
                    <div class="section-icon col s12 m12 l2">
                        <i class="fa fa-paper-plane-o"></i>
                    </div>
                    <div class="col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">

                        <h2>Contact</h2>
                        <div class="contact-form">
                            <div class="row">
                                <form action="/contact" method="post"
                                      data-toggle="validator">
                                    {{csrf_field()}}
                                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    <div class="input-field col s12">
                                        <label for="name" class="h4">Name *</label>
                                        <input type="text" class="form-control validate" id="name" name="name" required
                                               data-error="NEW ERROR MESSAGE">
                                    </div>
                                    <div class="input-field col s12">
                                        <label for="email" class="h4">Email *</label>
                                        <input name="email" type="email" class="form-control validate" id="email"
                                               required>
                                    </div>
                                    <div class="input-field col s12">
                                        <label for="message" class="h4 ">Message *</label>
                                        <textarea name="message" id="message"
                                                  class="form-control materialize-textarea validate"
                                                  required></textarea>
                                    </div>
                                    <button type="submit" id="form-submit" class="btn btn-success">Submit</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- =========================================
                  portfolio Website
                ==========================================-->

                <div class="section-wrapper z-depth-1 wow fadeIn" data-wow-delay="0.1s">
                    <div class="col s12 m12 l10 website right">
                        <div class="row">
                            <div class="col s12 m12 l6">
                                <span><a href="#">www.alrayhan.com</a></span>
                            </div>
                            <div class="col col s12 m12 l6">
                                <span><a href="#">www.dribble.com/rtralrayhan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </section>
    </div> <!-- end row -->
</div> <!-- end container -->

<!--=====================
        JavaScript
===================== -->
<!-- Jquery core js-->
<script src="assets/js/jquery.min.js"></script>

<!-- materialize js-->
<script src="assets/js/materialize.min.js"></script>

<!-- wow js-->
<script src="assets/js/wow.min.js"></script>

<!-- Map api -->
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyCRP2E3BhaVKYs7BvNytBNumU0MBmjhhxc"></script>

<!-- Masonry js-->

<script src="assets/js/validator.min.js"></script>

<script src="assets/js/jquery.mixitup.min.js"></script>

<!-- Customized js -->

<!-- =========================================================
    STYLE SWITCHER | ONLY FOR DEMO NOT INCLUDED IN MAIN FILES
===========================================================-->

<!-- Style switter js -->


</body>

<!-- Mirrored from demo.deviserweb.com/cv/contact.blade.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 20:46:33 GMT -->
</html>
