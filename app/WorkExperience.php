<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    protected $table = 'work_experiences';

    protected $fillable = ['job_title', 'company_name', 'started_at', 'ended_at', 'description'];


}
