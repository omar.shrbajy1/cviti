<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class NewVisitor extends Middleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $ip = request()->ip();
        $data = (array)\Location::get($ip);

        $countryCode = $data['countryCode'];
        $countryName = $data['countryName'];
        $cityName = $data['cityName'];
        try {
            \App\Visitor::create([
                'country_code' => $countryCode,
                'country_name' => $countryName,
                'city_name' => $cityName,
                'ip' => $ip,
                'date' => date("Y-m-d")
            ]);
        } catch (\Exception $e) {

        } finally {
            return $next($request);
        }
    }

}
