<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function index()
    {

        return view('admin.personal-info');
    }

    public function update(Request $request)
    {

        Auth::user()->update($request->all());
        return redirect('/admin/user');
    }

    public function coverLetter()
    {
        return view('admin.cover-letter');
    }

    public function updateCoverLetter(Request $request)
    {
        Auth::user()->update(['cover_letter' => $request['cover_letter']]);
        return redirect('admin/cover-letter');
    }

}
