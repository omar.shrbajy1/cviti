<?php

namespace App\Http\Controllers;

use App\Education;
use Illuminate\Http\Request;
use DataTables;
use Validator;

class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.education');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function DataTable()
    {
        return DataTables::of(Education::all())->addColumn('action', function ($item) {
            return '<i   data-id="' . $item->id . '" class="fa fa-pencil fa-2x" style="color:blue; cursor:pointer;margin-left: 9px;" aria-hidden="true"></i>' .
                '<i style="color:red; left:10px;cursor:pointer" data-id="' . $item->id . '" class="fa fa-trash-o fa-2x" aria-hidden="true"></i>';
        })->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'education_title' => 'required|string',
            'scientific_institution' => 'required|string',
            'degree' => 'required|string',
            'description' => 'nullable|min:15',
            'started_at' => 'required|date',
            'ended_at' => 'nullable|date'
        ])->validate();

        Education::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'education_title' => 'required|string',
            'scientific_institution' => 'required|string',
            'degree' => 'required|string',
            'description' => 'nullable|min:15',
            'started_at' => 'required|date',
            'ended_at' => 'nullable|date'
        ])->validate();


        Education::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Education::find($id)->delete();
    }
}
