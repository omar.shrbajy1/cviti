<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;

class LandingController extends Controller
{
    public function index()
    {
        return view('cv.index');
    }

    public function coverLetter()
    {
        return view('cv.cover-latter');
    }

    public function project()
    {
        return view('cv.project');
    }

    public function contact()
    {
        return view('cv.contact');
    }

    public function sendEmail(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'email|required',
            'message' => 'required'
        ])->validate();

        $data = array('name' => "Virat Gandhi");
//        return 1;
        $data = array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'messages' => $request->get('message')
        );
        Mail::send('emails.template',
            $data, function ($message) use ($request) {

                $message->from(
                    $request->get('email'),
                    $request->get('name')
                );
                $message->to('omar.shrbajy1@gmail.com', 'Omar Shrbaji');
                $message->subject('Omar Shrbaji');
            });

        //return "Send Mail Successfully!";

        return redirect('/contact');

    }


}
