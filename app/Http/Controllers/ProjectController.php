<?php

namespace App\Http\Controllers;

use App\Project;
use Faker\Provider\Company;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Image;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.project');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function DataTable()
    {
        return DataTables::of(Project::all())->addColumn('action', function ($item) {
            return '<i style="color:red; left:10px;cursor:pointer" data-id="' . $item->id . '" class="fa fa-trash-o fa-2x" aria-hidden="true"></i>';
        })->make(true);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'project_name' => 'required|string',
            'project_url' => 'nullable|url',
            'description' => 'nullable|min:15'
        ])->validate();

        $project = new Project;
        $photo = $request->file('photo');
        if ($photo) {
            $imagename = time() . '.' . $photo->getClientOriginalExtension();
            $destinationPath = public_path('/project-images');
            $image = Image::make($photo->getRealPath());
            $image->save($destinationPath . '/' . $imagename);
            $project->image = '/project-images' . '/' . $imagename;
        }

        $project->project_name = $request['project_name'];
        $project->project_url = $request['project_url'];
        $project->description = $request['description'];
        $project->save();

        return response()->json($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::find($id)->delete();
    }
}
