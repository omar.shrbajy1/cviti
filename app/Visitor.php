<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = ['country_name', 'country_code', 'date', 'ip', 'city_name'];
}
