<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'education';


    protected $fillable = ['education_title', 'scientific_institution', 'degree', 'started_at', 'ended_at', 'description'];
}
